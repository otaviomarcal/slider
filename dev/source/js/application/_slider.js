var showSlider = function(){
	var $slider 	= selector('.slider'),
		$pagination = selector('.pagination'),
		$pagLink 	= $pagination.getElementsByTagName('a'),
		$btnext 	= selector('.next'),
		$btprev 	= selector('.prev'),
		time 		= 5000,
		slideTimer;

	var settings = {
		slide: function(){
			el = selector('.active');

			if(el.nextElementSibling){
				addClass(el.nextElementSibling, 'active');

				settings.captions(el.nextElementSibling);
				el.classList.remove('active');
			}else{
				el.classList.remove('active');
				settings.firstSlide();
			}
		},

		captions: function(obj){
			var captions = obj.querySelector('img').getAttribute('alt');
			selector('figcaption').innerHTML = captions;
		},

		firstSlide: function(){
			el = selector('.slider a:first-child');
			addClass(el, 'active');
			this.captions(el);
		},

		btNext: function(){
			settings.clearTimer();
			el = selector('.active');
			
			if(el.nextElementSibling){
				addClass(el.nextElementSibling, 'active');
				settings.captions(el.nextElementSibling);
				el.classList.remove('active');
			}else{
				el.classList.remove('active');
				settings.firstSlide();
			}
		},

		btPrev: function(){
			settings.clearTimer();
			el = selector('.active');
			
			if(el.previousElementSibling){
				addClass(el.previousElementSibling, 'active');
				settings.captions(el.previousElementSibling);
				el.classList.remove('active');
			}else{
				el.classList.remove('active');						
				el = selector('.slider a:last-child');
				addClass(el, 'active');
				settings.captions(el);
			}
		},

		pagination: function(){
			active		= selector('.active'),
			count 		= $slider.getElementsByTagName('a').length,
			i 		   	= 0;

			for (; i < count; i++) $pagination.innerHTML += '<li><a href="#" title="">'+ (i+1) +'</a></li>';

			selector('.pagination').delegate('click', 'a', function($element, event){
				$element = ($element.innerHTML)-1;
			    settings.changeImage($element);
			});
		},

		changeImage: function(val) {
			settings.clearTimer();
			var $nodes = $slider.getElementsByTagName('a'),
				$count = $nodes.length,
				i	  = 0;

			for (; i < count; i++){
				if ($nodes[i].className == 'active') removeClass($nodes[i], 'active');
			}

			addClass($nodes[val], 'active');
			settings.captions($nodes[val]);
		},

		clearTimer: function() {
			clearInterval(slideTimer);
		},

		startTimer: function(e) {
			if( (e != undefined) && (e.tagName == 'SPAN') ){
				return false;
			}
			slideTimer = setInterval(settings.slide, time);

	  	}
	};

	settings.firstSlide();
	settings.captions(el);
	settings.pagination();
	settings.startTimer();
	
	//chama os mano tudo
	$btnext.addEventListener('click',settings.btNext,false);
	$btprev.addEventListener('click',settings.btPrev,false);

	$btnext.addEventListener('mouseover',settings.clearTimer,false);
	$btprev.addEventListener('mouseout',settings.startTimer,false);

	selector('.pagination').addEventListener('mouseover',settings.clearTimer,false);
	selector('.pagination').addEventListener('mouseout',settings.startTimer,false);	

	$slider.addEventListener('mouseover', settings.clearTimer, false);
	$slider.addEventListener('mouseout', settings.startTimer, false);
	
};